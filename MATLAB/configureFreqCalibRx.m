function prmFreqCalibRx = configureFreqCalibRx(platform, rfRxFreq)
% function prmFreqCalibRx = configureFreqCalibRx(platform, rfRxFreq, bbRxFreq)
% The function is called by sdruFrequencyCalibrationReceiver.m MATLAB
% script for System object initialization.

%   Copyright 2013-2014 The MathWorks, Inc.

switch platform
  case {'B200','B210'}
    prmFreqCalibRx.MasterClockRate = 20e6;  %Hz
    prmFreqCalibRx.Fs = 200e3; % sps
  case {'X300','X310'}
    prmFreqCalibRx.MasterClockRate = 120e6; %Hz
    prmFreqCalibRx.Fs = 240e3; % sps
  case {'N200/N210/USRP2'}
    prmFreqCalibRx.MasterClockRate = 100e6; %Hz
    prmFreqCalibRx.Fs = 200e3; % sps
  otherwise
    error(message('sdru:examples:UnsupportedPlatform', ...
      platform))
end

% SDRu Receiver System object
prmFreqCalibRx.RxCenterFrequency = rfRxFreq; 
prmFreqCalibRx.Gain              = 0;
prmFreqCalibRx.DecimationFactor  = ...
  prmFreqCalibRx.MasterClockRate/prmFreqCalibRx.Fs;

% prmFreqCalibRx.FrameLength       = 4096; 
prmFreqCalibRx.FrameLength       = 4000; 
prmFreqCalibRx.TotalFrames       = 2000;
prmFreqCalibRx.RxSineFrequency   = 100; 
prmFreqCalibRx.OutputDataType    = 'double'; 

% Coarse Freq Offset estimation 
prmFreqCalibRx.FocFFTSize        = 2048;    

% EOF
