clear all
close all
clc

%% usrp_transceiver -- LOST group USRP transceiver script
% This script runs the LOST group USRP transceiver, which is an amalgam of
% the usrp_transmitter.m and usrp_receiver.m files. This file is usually
% outdated compared to the separate transmitter and receiver files because
% this was the file used by individuals in full duplex mode. 

% As discussed in our leave_behind.pdf, we initially worked individually as 
% 3 people on 3 separate USRP's in full duplex mode (the transmitter was also the
% receiver). However, we soon found out that this didn't give us the "real
% world" effects of phase, time, and frequency offset because the receiver
% and transmitter were on the same clock. As such, this file became the
% "work in progress" file amongst team members.

%% Discover Radio
% Discover radio(s) connected to your computer. This example uses the first
% USRP(R) radio found using the |findsdru| function. Check if the radio is
% available and record the radio type. If no available radios are found,
% the example uses a default configuration for the system but does not run
% the main loop.

connectedRadios = findsdru;
if strncmp(connectedRadios(1).Status, 'Success', 7)
  radioFound = true;
  platform = connectedRadios(1).Platform;
  switch connectedRadios(1).Platform
    case {'B200','B210'}
      address = connectedRadios(1).SerialNum;
    case {'N200/N210/USRP2','X300','X310'}
      address = connectedRadios(1).IPAddress;
  end
else
  radioFound = false;
  address = '192.168.10.20';
  platform = 'N200/N210/USRP2';     
end

%% Initialization
% Set the properties of the sine wave source, the SDRu transmitter, and the
% spectrum analyzer System object.

bbTxFreq = 100;    % Transmitted baseband frequency
rfTxFreq = 420e6; % Nominal RF transmit center frequency
rfTxFreq = 421e6; % Nominal RF transmit center frequency

prmFreqCalibTx = configureFreqCalibTx(platform, rfTxFreq);

%% Initialization
% Baseband and RF configuration
rfRxFreq           = 420e6;  % Nominal RF receive center frequency
bbRxFreq           = 100;     % Received baseband sine wave frequency

prmFreqCalibRx = configureFreqCalibRx(platform, rfRxFreq);

hSineSource = dsp.SineWave (...
    'Frequency',           100, ...
    'Amplitude',           prmFreqCalibTx.SineAmplitude,...
    'ComplexOutput',       prmFreqCalibTx.SineComplexOutput, ...
    'SampleRate',          prmFreqCalibTx.Fs, ...
    'SamplesPerFrame',     prmFreqCalibTx.SineFrameLength, ...
    'OutputDataType',      prmFreqCalibTx.SineOutputDataType);

% The host computer communicates with the USRP(R) radio using the SDRu
% transmitter System object. B200 and B210 series USRP(R) radios are
% addressed using a serial number while USRP2, N200, N210, X300 and X310
% radios are addressed using an IP address. The parameter structure,
% prmFreqCalibTx, sets the CenterFrequency, Gain, and InterpolationFactor
% arguments.

% Set up radio tx object to use the found radio
switch platform
  case {'B200','B210'}
    radioTx = comm.SDRuTransmitter( ...
      'Platform',             platform, ...
      'SerialNum',            address, ...
      'MasterClockRate',      prmFreqCalibTx.MasterClockRate, ...
      'CenterFrequency',      prmFreqCalibTx.USRPTxCenterFrequency, ...
      'Gain',                 prmFreqCalibTx.USRPGain,...
      'InterpolationFactor',  prmFreqCalibTx.USRPInterpolationFactor)
  case {'X300','X310'}
    radioTx = comm.SDRuTransmitter( ...
      'Platform',             platform, ...
      'IPAddress',            address, ...
      'MasterClockRate',      prmFreqCalibTx.MasterClockRate, ...
      'CenterFrequency',      prmFreqCalibTx.USRPTxCenterFrequency, ...
      'Gain',                 prmFreqCalibTx.USRPGain,...
      'InterpolationFactor',  prmFreqCalibTx.USRPInterpolationFactor)
  case {'N200/N210/USRP2'}
    radioTx = comm.SDRuTransmitter( ...
      'Platform',             platform, ...
      'ClockSource',          'External',...
      'IPAddress',            '192.168.10.22', ...
      'CenterFrequency',      prmFreqCalibTx.USRPTxCenterFrequency, ...
      'Gain',                 prmFreqCalibTx.USRPGain,...
      'InterpolationFactor',  prmFreqCalibTx.USRPInterpolationFactor)
end

% This example communicates with the USRP(R) board using the SDRu receiver
% System object. B200 and B210 series USRP(R) radios are addressed using a
% serial number while USRP2, N200, N210, X300 and X310 radios are addressed
% using an IP address. The parameter structure, prmFreqCalibRx, sets the
% CenterFrequency, Gain, InterpolationFactor, and SamplesPerFrame
% arguments.
switch platform
  case {'B200','B210'}
    radioRx = comm.SDRuReceiver(...
        'Platform',         platform, ...
        'SerialNum',        address, ...
        'MasterClockRate',  prmFreqCalibRx.MasterClockRate, ...
        'CenterFrequency',  prmFreqCalibRx.RxCenterFrequency,...
        'Gain',             prmFreqCalibRx.Gain, ...
        'DecimationFactor', prmFreqCalibRx.DecimationFactor,...
        'SamplesPerFrame',  prmFreqCalibRx.FrameLength,...
        'OutputDataType',   prmFreqCalibRx.OutputDataType)  
  case {'X300','X310'}
    radioRx = comm.SDRuReceiver(...
        'Platform',         platform, ...
        'IPAddress',        address, ...
        'MasterClockRate',  prmFreqCalibRx.MasterClockRate, ...
        'CenterFrequency',  prmFreqCalibRx.RxCenterFrequency,...
        'Gain',             prmFreqCalibRx.Gain, ...
        'DecimationFactor', prmFreqCalibRx.DecimationFactor,...
        'SamplesPerFrame',  prmFreqCalibRx.FrameLength,...
        'OutputDataType',   prmFreqCalibRx.OutputDataType)  
  case {'N200/N210/USRP2'}
    radioRx = comm.SDRuReceiver(...
        'Platform',         platform, ...
        'IPAddress',        '192.168.10.3', ...
        'CenterFrequency',  prmFreqCalibRx.RxCenterFrequency,...
        'Gain',             prmFreqCalibRx.Gain, ...
        'DecimationFactor', prmFreqCalibRx.DecimationFactor,...
        'SamplesPerFrame',  prmFreqCalibRx.FrameLength,...
        'OutputDataType',   prmFreqCalibRx.OutputDataType, ...
        'ClockSource',      'External')  
end

% Use dsp.SpectrumAnalyzer to display the spectrum of the transmitted
% signal.
% hSpectrumAnalyzer = dsp.SpectrumAnalyzer(...
%     'Name',                'Frequency of the Sine waveform sent out',...
%     'Title',               'Frequency of the Sine waveform sent out',...
%     'FrequencySpan',       'Full', ...
%     'SampleRate',           prmFreqCalibTx.Fs, ...
%     'YLimits',              [-70,30],...
%     'SpectralAverages',     50, ...
%     'FrequencySpan',        'Start and stop frequencies', ...
%     'StartFrequency',       -100e3, ...
%     'StopFrequency',        100e3,...
%     'Position',             figposition([50 30 32 40]));

hSpectrumAnalyzer = dsp.SpectrumAnalyzer(...
    'Name',                          'Actual Frequency Offset',...
    'Title',                         'Actual Frequency Offset', ...
    'SpectrumType',                  'Power density',...
    'FrequencySpan',                 'Full', ...
    'SampleRate',                     prmFreqCalibRx.Fs, ...
    'YLimits',                        [-130,20],...
    'SpectralAverages',               50, ...
    'FrequencySpan',                  'Start and stop frequencies', ...
    'StartFrequency',                 -100e3, ...
    'StopFrequency',                  100e3,...
    'Position',                       figposition([50 30 30 40]));


load('final_wf');
load('canned_wf');
final_wf = (canwf-1/2)*2;

asdf = final_wf .* -1; % 0 = 1, 1 = -1 in BPSK

% repeat it to be size of frame
% temp = repmat(asdf, round(200e3/length(asdf)), 1);
% temp = temp(1:200e3);
temp = asdf;

% sinewave =  step(hSineSource); % generate sine wave
% temp = sinewave;


rx_ctr = 0;
rx_correct = 0;
%% Stream Processing                   
%  Loop until the example reaches the target number of frames.

% Check for the status of the USRP(R) radio
if radioFound
%     for iFrame = 1: prmFreqCalibTx.TotalFrames1
    for iFrame = 1: 1e6

        
        if exist('C:\LOST\tx.txt', 'file') == 2
            fid = fopen('C:\LOST\tx.txt', 'r');
            txmsg = fgetl(fid);
            %pause(1);
%             txmsg = textread('C:\LOST\tx.txt', '%s');
            fclose('all');
            %delete('C:\LOST\tx.txt');
        
            dec_in = ones(1, 30)*32; % msg is 30 bits; 32 is ascii ' '
            dec_in(1:length(txmsg)) = abs(txmsg); % coded message

            bin_in = dec2bin(dec_in,8)=='1'; % msg in binary
            txbin = bin_in';
            txbin = txbin(:); % tx message in binary
            txbpsk = (txbin-1/2)*2; % transform from [0,1] to [-1,1] bpsk
            txbpsk = txbpsk .* -1; % bpsk "0" = 1, "1" = -1, so flip signs
            % make sequence 3 bits long for redundancy (upsample)
            pattern=[];
            for k=1:length(txbpsk)
                if txbpsk(k)==-1
                    sig=ones(1,3).*-1;
                else
                    sig=ones(1,3);
                end
                pattern=[pattern sig];
            end
            txbpsk = pattern';
                   % preamble
            temp = [asdf; txbpsk];
        
% % Troubleshooting with sinewave:
%           sinewave =  step(hSineSource); % generate sine wave
%           step	; % transmit to USRP(R) radio

%           % Comment in/out to use radioTx
          step(radioTx, temp); % transmit to USRP(R) radio
          %step(radioTx, temp); % transmit to USRP(R) radio
          %step(radioTx, temp); % transmit to USRP(R) radio
          
          [rxSig, len ] = step(radioRx);
          if len > 0
            try 
                rx_ctr = rx_ctr + 1;
                thresh = rxSig > 0; % threshold the data to determine 0/1   

                % time align frames to find peak of reference versus sampled
                % frame
                [corrchk lags] = xcorr((thresh-1/2)*2, final_wf); % correlate
                [mx ind] = max(abs(corrchk)); 
                newthresh = thresh;

                % Decoding using correlation peaks
                if ind < length(newthresh) % find # of samples correlation is off
                    decodedRx = newthresh((length(newthresh)-1)-ind:(length(newthresh)-1)-ind+length(final_wf)-1); % take peak of xcorr
                else
                    decodedRx = newthresh(ind-(length(newthresh)-1):ind-(length(newthresh)-1)+length(final_wf)-1); % take peak of xcorr
                end

                % This is just initialization vector
                decodedown = zeros(length(decodedRx)/3, 1);

                % Down sample from 3:1 bc tx is upsampled 1:3. 
                for kkk = 1:3:length(decodedRx) % "majority rules" 2/3 decoding
                   temp2 = decodedRx(kkk:kkk+2);
                   decodedown(ceil(kkk/3)) = sum(temp2) > 1;
                end
                final_wf_dig = (final_wf+1)/2; % digital [-1,1] to [0,1]

                % check bit differences. Also check the bit flip (~decodedown)
                chk1 = sum(abs(decodedown - final_wf_dig(1:3:end)));
                chk2 = sum(abs(~decodedown - final_wf_dig(1:3:end)));
                
                if chk1 == 0 || chk2 == 0
                   rx_correct = rx_correct + 1; 
                end
                rx_correct / rx_ctr 

                % Just a line so we could set a breakpoint for what the
                % reference and signal look like when they're aligned
                if chk1 == 0 || chk2 == 0
                    testerino = 1;                    
                    if ind < length(newthresh) % find # of samples correlation is off
                        rxmsg = newthresh((length(newthresh)-1)-ind+length(final_wf):(length(newthresh)-1)-ind+length(final_wf) + 30*8*3-1); % take data after preamble
                    else
                        rxmsg = newthresh(ind-(length(newthresh)-1)+length(final_wf):ind-(length(newthresh)-1)+length(final_wf) + 30*8*3-1); % take data after preamble
                    end
                    % initialize vector
                    rxmsgdown = zeros(length(rxmsg)/3, 1);

                    for kkk = 1:3:length(rxmsg) % "majority rules" 2/3 decoding
                        temp3 = rxmsg(kkk:kkk+2);
                        rxmsgdown(ceil(kkk/3)) = sum(temp3) > 1;
                    end

                    if chk1 == 0
                        % do nothing, msg should be fine
                    elseif chk2 == 0
                        rxmsgdown = ~rxmsgdown;
                    end

                    fliprxmsgdown = ~rxmsgdown;

                    rxreshaped = logical(reshape(rxmsgdown, [8 length(rxmsgdown)/8 ]));
                    rxreshaped2 = logical(reshape(fliprxmsgdown, [8 length(fliprxmsgdown)/8]));

                    temp4 = bin2dec(num2str(rxreshaped')); % temp string to convert to decimals
                    temp5 = bin2dec(num2str(rxreshaped2')); % temp string to converet to decimals

                    decoded_str = native2unicode(temp4', 'ascii');
                    decoded_str2 = native2unicode(temp5', 'ascii'); 

                    fid = fopen('C:\LOST\rx.txt', 'w');
                    fwrite(fid, decoded_str);
                    pause(0.1);
                    fclose(fid);

                end
                % For troubleshooting
                step(hSpectrumAnalyzer, rxSig);
            catch
            end
          end
        else
            % do nothing if no tx.txt
            disp('no tx.txt file found') % can comment out later
        end
    end
    % Display the spectrum after the simulation.
%     step(hSpectrumAnalyzer, temp);
else
    warning(message('sdru:sysobjdemos:MainLoop'))
end

%% Release System Objects
release (hSineSource);
release (radioTx);
release (radioRx);
release (hSpectrumAnalyzer);
clear radio

%% Conclusion
% In this example, you used Communications System Toolbox(TM) System objects
% to build a signal source to send a reference tone at 100 Hz. This signal
% is to be used as a calibration signal for a USRP(R) receiver.
%
%
%% Appendix
% The following scripts are used in this example.
%
% * <matlab:edit('configureFreqCalibTx.m') configureFreqCalibTx.m>
%
%
%% Copyright Notice
% Universal Software Radio Peripheral(R) and USRP(R) are trademarks of
% National Instruments Corp.
displayEndOfDemoMessage(mfilename)