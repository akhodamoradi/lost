function prmFreqCalibTx = configureFreqCalibTx(platform,rfTxFreq)
% function prmFreqCalibTx = configureFreqCalibTx(platform,rfTxFreq,bbTxFreq)
% Configure parameters for SDRu Frequency Calibration Transmitter example.

%   Copyright 2013-2014 The MathWorks, Inc.

switch platform
  case {'B200','B210'}
    prmFreqCalibTx.MasterClockRate = 20e6;  %Hz
    prmFreqCalibTx.Fs = 200e3; % sps
  case {'X300','X310'}
    prmFreqCalibTx.MasterClockRate = 120e6; %Hz
    prmFreqCalibTx.Fs = 240e3; % sps
  case {'N200/N210/USRP2'}
    prmFreqCalibTx.MasterClockRate = 100e6; %Hz
    prmFreqCalibTx.Fs = 200e3; % sps
  otherwise
    error(message('sdru:examples:UnsupportedPlatform', ...
      platform))
end

% SDRu Transmitter System object
prmFreqCalibTx.USRPInterpolationFactor = ...
  prmFreqCalibTx.MasterClockRate/prmFreqCalibTx.Fs; 

prmFreqCalibTx.USRPFrameLength         = 4000;     % samples
prmFreqCalibTx.USRPTxCenterFrequency   = rfTxFreq; % Hz
prmFreqCalibTx.USRPGain                = 0;       % dB
prmFreqCalibTx.TotalFrames             = 2000;     % frames

% Sine wave
prmFreqCalibTx.SineAmplitude           = 0.1;
% prmFreqCalibTx.SineFrequency           = bbTxFreq; % 100Hz
prmFreqCalibTx.SineComplexOutput       = true;
prmFreqCalibTx.SineOutputDataType      = 'double';
prmFreqCalibTx.SineFrameLength         = 4000;
