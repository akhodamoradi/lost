﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.IO;
using System.Windows.Forms;

namespace LOSTConsole
{
    public partial class Main : Form
    {
        Boolean showcursor = true; // false screen capture
        int line = 0;
        String cursor = ">: ";
        String path = @"C:\LOST\";
        String tx = "tx.txt";
        String rx = "rx.txt";
        String input = string.Empty;
        bool diff = true;
        bool printed = false;

        // Different caret info
        [DllImport("user32.dll")]
        static extern bool CreateCaret(IntPtr hWnd, IntPtr hBitmap, int nWidth, int nHeight);
        [DllImport("user32.dll")]
        static extern bool ShowCaret(IntPtr hWnd);
        [DllImport("user32.dll")]
        static extern bool HideCaret(IntPtr hWnd);
        
        public Main()
        {
            InitializeComponent();
            textBox1.SelectionStart = textBox1.Text.Length;
            backgroundWorker1.RunWorkerAsync();
        }

        private void textBox1_GotFocus(Object sender, EventArgs e)
        {
            if (!showcursor)
            {
                HideCaret(textBox1.Handle);
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13) // Enter
            {
                try
                {
                    if (textBox1.Lines[line] == cursor) // Nothing entered
                    {
                        e.Handled = true;
                    }
                    else
                    {
                        // Write text to a file
                        using (StreamWriter writetext = new StreamWriter(path + tx))
                        {
                            writetext.WriteLine(textBox1.Lines[line].Replace(cursor, String.Empty));
                        }

                        // Setup console
                        line += 2;
                        textBox1.Text = textBox1.Text +
                            Environment.NewLine +
                            Environment.NewLine +
                            cursor;
                        textBox1.SelectionStart = textBox1.Text.Length;
                        textBox1.SelectionLength = 0;

                        e.Handled = true;

                        // Move to the end
                        textBox1.SelectionStart = textBox1.Text.Length;
                        textBox1.SelectionLength = 0;
                        textBox1.ScrollToCaret();
                    }   
                }
                catch
                {
                    e.Handled = true;
                }
            }
            else if(e.KeyChar == (char)8) // Backspace
            {
                try
                {
                    if (textBox1.Lines[line] == cursor) // Nothing entered
                    {
                        e.Handled = true;
                    }
                }
                catch
                {
                    e.Handled = true;
                }
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (File.Exists(path + rx))
                {
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        input = string.Empty;

                        using (StreamReader readtext = new StreamReader(path + rx))
                        {
                            input = readtext.ReadToEnd();
                            if (diff)
                            {
                                line += 2;

                                if (line == 2) // If you rx before tx
                                {
                                    // Print the rx message
                                    textBox1.Text = input + 
                                        Environment.NewLine +
                                        Environment.NewLine +
                                        cursor;

                                    printed = true;
                                    input = string.Empty;
                                }
                                else
                                {
                                    // Print the rx message
                                    textBox1.Text = textBox1.Text.Remove(textBox1.Text.LastIndexOf(Environment.NewLine)) +
                                        Environment.NewLine +
                                        input + 
                                        Environment.NewLine +
                                        Environment.NewLine +
                                        cursor;

                                    printed = true;
                                    input = string.Empty;
                                }

                                // Move to the end
                                textBox1.SelectionStart = textBox1.Text.Length;
                                textBox1.SelectionLength = 0;
                                textBox1.ScrollToCaret();
                            }
                        }
                    });
                }
                if (printed)
                {
                    try
                    {
                        // Delete the rx message
                        File.Delete(path + rx);
                        printed = false;
                        input = string.Empty;
                    }
                    catch
                    {

                    }
                }

                System.Threading.Thread.Sleep(2000);
            }
        }

        private void Main_Shown(object sender, EventArgs e)
        {
            if (showcursor)
            {
                CreateCaret(textBox1.Handle, IntPtr.Zero, 10, 12);
                ShowCaret(textBox1.Handle);
            }
            else
            {
                HideCaret(textBox1.Handle);
            }
        }
    }
}
